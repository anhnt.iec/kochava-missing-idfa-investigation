# Hướng dẫn

- Tạo một copy của template sau cho mỗi game https://docs.google.com/spreadsheets/d/1eR-c-GJno7330PKVT-j8FEgC4Adb6KHg18gr7Is6QXk/edit#gid=1765400499
- Marketing điền các thông tin của các cột: `Platform`, `Country`, `Ads Network`, `From`, `To`, `Rev ROAS - FACEBOOK`, `Rev ROAS - UNATTRIBUTED`, `Rev Applovin`
- Query: Với mỗi platform/nước chạy query tương ứng cho android hoặc ios, thay giá trị các biến và thay tên bảng theo từng game
- Chú ý phải tìm tên nước tương ứng trên Firebase cho đúng với mỗi mã nước để thay vào `countryName`

# Danh sách countryName để đối chiếu

```sql
DECLARE startDate STRING DEFAULT "20200618";
DECLARE endDate STRING DEFAULT "20200620";

SELECT geo.country as countryName
FROM `ball-sort-puzzle.analytics_219019016.events_*`
GROUP BY countryName
```

# iOS

```sql
DECLARE startDate STRING DEFAULT "20200618";
DECLARE endDate STRING DEFAULT "20200620";
DECLARE countryCode STRING DEFAULT "KE";
DECLARE countryName STRING DEFAULT "Kenya";

WITH 
  Revenues AS (
    SELECT UPPER(IDFA) as IDFA, SUM(Revenue) as Revenue
    FROM `ball-sort-puzzle.data_sync.applovin_user_revenue_ios_*` 
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
    GROUP BY IDFA
  ),
  IDFAs_kochava AS (
    SELECT UPPER(install_devices_idfa) as IDFA
    FROM `ball-sort-puzzle.data_sync.kochava_install_report_ios_*` 
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
      AND installgeo_country_code = countryCode
    GROUP BY IDFA
  ),
  IDFAs_firebase AS (
    SELECT UPPER(device.advertising_id) as IDFA
    FROM `ball-sort-puzzle.analytics_219019016.events_*`
      , UNNEST(event_params) as params
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
      AND geo.country = countryName
      AND device.operating_system = "IOS"
    GROUP BY IDFA
  ),
  IDFAs_count_kochava AS (
    SELECT COUNT(DISTINCT IDFA) as IDFAs_Kochava, countryCode as country
    FROM IDFAs_kochava
  ),
  IDFAs_count_firebase AS (
    SELECT COUNT(DISTINCT IDFA) as IDFAs_Firebase, countryCode as country
    FROM IDFAs_firebase
  ),
  Revenue_kochava AS (
    SELECT SUM(Revenue) as Rev_Kochava, countryCode as country
    FROM Revenues
    WHERE IDFA IN (SELECT IDFA FROM IDFAs_kochava)
  ),
  Revenue_firebase AS (
    SELECT SUM(Revenue) as Rev_Firebase, countryCode as country
    FROM Revenues
    WHERE IDFA IN (SELECT IDFA FROM IDFAs_firebase)
  )
SELECT
  Rev_Kochava,
  Rev_Firebase,
  (Rev_Kochava/Rev_Firebase - 1) as Delta_2,
  IDFAs_Kochava,
  IDFAs_Firebase,
  (IDFAs_Kochava/IDFAs_Firebase - 1) as Delta_3
FROM Revenue_kochava
JOIN Revenue_firebase USING (country)
JOIN IDFAs_count_kochava USING (country)
JOIN IDFAs_count_firebase USING (country)
```

# Android

```sql
DECLARE startDate STRING DEFAULT "20200618";
DECLARE endDate STRING DEFAULT "20200620";
DECLARE countryCode STRING DEFAULT "LT";
DECLARE countryName STRING DEFAULT "Lithuania";

WITH 
  Revenues AS (
    SELECT UPPER(IDFA) as IDFA, SUM(Revenue) as Revenue
    FROM `ball-sort-puzzle.data_sync.applovin_user_revenue_android_*` 
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
    GROUP BY IDFA
  ),
  IDFAs_kochava AS (
    SELECT UPPER(install_devices_adid) as IDFA
    FROM `ball-sort-puzzle.data_sync.kochava_install_report_android_*` 
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
      AND installgeo_country_code = countryCode
      AND UPPER(install_devices_adid) in (select IDFA from revenues)
    GROUP BY IDFA
  ),
  IDFAs_firebase AS (
    SELECT UPPER(device.advertising_id) as IDFA
    FROM `ball-sort-puzzle.analytics_219019016.events_*`
      , UNNEST(event_params) as params
    WHERE _TABLE_SUFFIX BETWEEN startDate AND endDate
      AND geo.country = countryName
      AND UPPER(device.advertising_id) in (select IDFA from revenues)
      AND device.operating_system = "ANDROID"
    GROUP BY IDFA
  ),
  IDFAs_count_kochava AS (
    SELECT COUNT(DISTINCT IDFA) as IDFAs_Kochava, countryCode as country
    FROM IDFAs_kochava
  ),
  IDFAs_count_firebase AS (
    SELECT COUNT(DISTINCT IDFA) as IDFAs_Firebase, countryCode as country
    FROM IDFAs_firebase
  ),
  Revenue_kochava AS (
    SELECT SUM(Revenue) as Rev_Kochava, countryCode as country
    FROM Revenues
    WHERE IDFA IN (SELECT IDFA FROM IDFAs_kochava)
  ),
  Revenue_firebase AS (
    SELECT SUM(Revenue) as Rev_Firebase, countryCode as country
    FROM Revenues
    WHERE IDFA IN (SELECT IDFA FROM IDFAs_firebase)
  )
SELECT
  Rev_Kochava,
  Rev_Firebase,
  (Rev_Kochava/Rev_Firebase - 1) as Delta_2,
  IDFAs_Kochava,
  IDFAs_Firebase,
  (IDFAs_Kochava/IDFAs_Firebase - 1) as Delta_3
FROM Revenue_kochava
JOIN Revenue_firebase USING (country)
JOIN IDFAs_count_kochava USING (country)
JOIN IDFAs_count_firebase USING (country)
```
